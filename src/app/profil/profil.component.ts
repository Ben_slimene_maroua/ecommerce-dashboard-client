import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order.service';
import { ListproductService } from '../listproduct.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  userconnect= JSON.parse(localStorage.getItem("userconnect")!);
  listorder: any;
  constructor(private orderservice:OrderService, private  listprodservice: ListproductService) { }

  ngOnInit(): void {
    this.getorder();
  }
getorder(){
    return this.orderservice.getOrder().subscribe((res:any)=>{
  this.listorder=res['data'];
  const orderByClient= this.listorder.filter((el:any)=> el.client == this.userconnect._id)
  this.listorder= orderByClient;
  console.log("order:", this.listorder)
    })
  }
}

