import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { CartService } from '../cart.service';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  userconnect= JSON.parse(localStorage.getItem("userconnect")!);
  cart_items= JSON.parse(localStorage.getItem("cart_items")!);
 items=[] as any;
  totitem:any;
  orderForm: FormGroup;
  totalPrice:any;
  totQte:any
  constructor(private formbuilder:FormBuilder, private route: Router,private cartservice: CartService,private orderservice: OrderService) { }

  ngOnInit(): void {
  this.cartservice.loadCart();
  this.items=this.cartservice.getItems();

  this.orderForm= this.formbuilder.group({
    client:['',Validators.required],
    quantite:['',Validators.required],
    prix:['',Validators.required],
    payer:['',Validators.required],
    itemorders:['',Validators.required]

  })
  this.totalQte();
  this.gettotal()
  }

addOrder(){
  this.orderForm.patchValue({
    client:this.userconnect._id,
    quantite:this.totQte,
    prix: this.totalPrice ,
    payer:true,
    itemorders:this.items,
  })
  console.log(this.items)
  this.orderservice.createOrder(this.orderForm.value).subscribe((res:any)=>{
    console.log(res)
    Swal.fire("order added")
  })
  this.route.navigateByUrl('/facture')
}


  total(item:any){

    let total=0;
    
      total = Number(item.prix)* Number(item.quantite);
    
    this.totitem=total;
    return this.totitem;
  
  }
  totalQte()
  {
    let total=0;
    this.items.forEach((element:any) => {
      total +=  Number(element.quantite);
    });
    this.totQte=total;
    return this.totQte;
      
      
    }
    gettotal(){
      let total=0;
      this.items.forEach((element:any) => {
        total += Number(element.prix)* Number(element.quantite);
      });
      this.totalPrice=total;
      return this.totalPrice;
        
      }

}
