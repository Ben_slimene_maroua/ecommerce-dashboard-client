import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items=[] as any;
  constructor() { }
 
  addTocart(addedItem:any){
    this.items.push(addedItem);
    this.saveCart();
  }

  getItems(){
    return this.items;
  }
  loadCart(){
    this.items= JSON.parse(localStorage.getItem("cart_items")!) ?? [];

  }
  saveCart(){
    localStorage.setItem('cart_items',JSON.stringify(this.items));
  }
  removeItem(item:any){
    console.log(this.items)
    const index=this.items.findIndex((o:any)=>o._id===item._id);
  //findindex return -1 si l id n existe pas
    if (index > -1){
      this.items.splice(index,1);
      this.saveCart();
    }
  }

itemInCart (item:any): boolean{
  return this.items.findIndex((o:any)=>o._id===item._id) > -1 ;
}

}
