import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckoutComponent } from './checkout/checkout.component';
import { DetailcartComponent } from './detailcart/detailcart.component';
import { DetailproductComponent } from './detailproduct/detailproduct.component';
import { FactureComponent } from './facture/facture.component';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { ListproductComponent } from './listproduct/listproduct.component';
import { LoginComponent } from './login/login.component';
import { ProfilComponent } from './profil/profil.component';
import { RegistreComponent } from './registre/registre.component';


const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'',component:HomeComponent,children:[
    {path:'',component:LayoutComponent},
    {path:'listprod',component:ListproductComponent},
    {path:'detailproduct/:id',component:DetailproductComponent},
    {path:'detailcart',component:DetailcartComponent},
    {path:'checkout',component:CheckoutComponent},
    {path:'registre',component:RegistreComponent},
    {path:'facture',component:FactureComponent},
    {path:'profil',component:ProfilComponent}
  
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
