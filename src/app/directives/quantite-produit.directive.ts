import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appQuantiteProduit]'
})
export class QuantiteProduitDirective {
  //communication entre directive et detailproduct donc on travail avec @input
@Input('qte') qte: any;
@Input('qteuser') qteuser: any;

  constructor(private render:Renderer2, private el: ElementRef) { } //elementRef: element html qui sera changé
  @HostListener("keyup") //evenement ici click souris
  setclick(){
    if (this.qteuser<=this.qte){
      this.render.setStyle(this.el.nativeElement,"backgroundColor","green") // render pour changer style
      
    }
    else{
      this.render.setStyle(this.el.nativeElement,"backgroundColor","red")
    }
  }

}
