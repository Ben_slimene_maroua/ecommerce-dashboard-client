import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListproductService {

  constructor(private http:HttpClient) { }
  getallproducts(){
    return this.http.get(`${environment.baseUrl}/product/afficheProduct`)
  }
  getproduct(id:any){
    return this.http.get(`${environment.baseUrl}/product/getproductById/${id}`);
  }
  
}
