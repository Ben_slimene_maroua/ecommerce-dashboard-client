import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListproductComponent } from './listproduct/listproduct.component';
import { RecherchePipe } from './pipes/recherche.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { DetailproductComponent } from './detailproduct/detailproduct.component';
import { DetailcartComponent } from './detailcart/detailcart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { RegistreComponent } from './registre/registre.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { FactureComponent } from './facture/facture.component';
import { ProfilComponent } from './profil/profil.component';
import { Ng5SliderModule } from 'ng5-slider';
import { QuantiteProduitDirective } from './directives/quantite-produit.directive';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    LayoutComponent,
    HomeComponent,
    LoginComponent,
    ListproductComponent,
    RecherchePipe,
    DetailproductComponent,
    DetailcartComponent,
    CheckoutComponent,
    RegistreComponent,
    FactureComponent,
    ProfilComponent,
    QuantiteProduitDirective,
    

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    SlickCarouselModule,
    Ng5SliderModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
