import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 items=[] as any;
 totalPrice: any;
  constructor(private cartservice: CartService) { }

  ngOnInit(): void {
  this.cartservice.loadCart();
  this.items=this.cartservice.getItems();
  }
  gettotal(){
  let total=0;
  this.items.forEach((element:any) => {
    total += Number(element.prix)* Number(element.quantite);
  });
  this.totalPrice=total;
  return this.totalPrice;
    
  }
  removeitem(item:any){
    return this.cartservice.removeItem(item)
    this.items=this.cartservice.getItems();
  }
  Isconnect(){
  return localStorage.getItem('state')=='0'? true : false
  }
  logout(){
    localStorage.clear();
  }
}
