import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }
  createOrder(order:any){
  return   this.http.post(`${environment.baseUrl}/order/createorder`,order)
  }
  getOrder(){
    return this.http.get(`${environment.baseUrl}/order/afficheorder`)
  }
}

