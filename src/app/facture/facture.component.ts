import { Component, OnInit } from '@angular/core';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { CartService } from '../cart.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-facture',
  templateUrl: './facture.component.html',
  styleUrls: ['./facture.component.css']
})
export class FactureComponent implements OnInit {
  userconnect= JSON.parse(localStorage.getItem("userconnect")!);
  cart_items= JSON.parse(localStorage.getItem("cart_items")!);
 items=[] as any;
 totalPrice:any;
 totitem:any;

 // ajouter la date 
 today: Date = new Date();
 pipe = new DatePipe('en-US');
 todayWithPipe : any;

 
 
  constructor(private cartservice: CartService) { }

  ngOnInit(): void {
  // pour ajouter la date dans la facture
    this.todayWithPipe = this.pipe.transform(Date.now(), 'd/M/yy, h:mm a');

    this.cartservice.loadCart();
    this.items=this.cartservice.getItems();

 

    this.gettotal();
  }

  gettotal(){
    let total=0;
    this.items.forEach((element:any) => {
      total += Number(element.prix)* Number(element.quantite);
    });
    this.totalPrice=total;
    return this.totalPrice;
      
    }
    total(item:any){

      let total=0;
      
        total = Number(item.prix)* Number(item.quantite);
      
      this.totitem=total;
      return this.totitem;
    
    }
    
  public openPDF(): void {
    let DATA: any = document.getElementById('htmlData');
    html2canvas(DATA).then((canvas) => {
      let fileWidth = 208;
      let fileHeight = (canvas.height * fileWidth) / canvas.width;
      const FILEURI = canvas.toDataURL('image/png');
      let PDF = new jsPDF('p', 'mm', 'a4');
      let position = 0;
      PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight);
      PDF.save('facture.pdf');
    });
  }

  facture() {
    let facture = 0;
    facture = (this. gettotal()/100)*80 + (this. gettotal()/100)*10;
    return facture;
  }
}
