import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MustMatch } from 'helper/mustmatch';
import Swal from 'sweetalert2';
import { RegistreService } from '../registre.service';

@Component({
  selector: 'app-registre',
  templateUrl: './registre.component.html',
  styleUrls: ['./registre.component.css']
})
export class RegistreComponent implements OnInit {
regForm: FormGroup;
submitted=false;
fileToUpload:Array<File>=[]; 
  constructor(private registreservice:RegistreService , private formbuilder: FormBuilder, private route: Router) { }

  ngOnInit(): void {
    this.regForm=this.formbuilder.group({
      nom:['',Validators.required],
      prenom:['',Validators.required],
      adresse:['',Validators.required],
      email:['',[Validators.required, Validators.email]],
      password:['',[Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      image:['',Validators.required],
     
    },
    {
      validator: MustMatch('password', 'confirmPassword')
    } );
  }
  get f() { return this.regForm.controls; }
  handleFileInput(files:any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log(this.fileToUpload)
  }


  onSubmit() {
      this.submitted = true;

      let formdata= new FormData();
      formdata.append("nom",this.regForm.value.nom);
      formdata.append("prenom",this.regForm.value.prenom);
      formdata.append("adresse",this.regForm.value.adresse);
      formdata.append("email",this.regForm.value.email);
      formdata.append("password",this.regForm.value.password);
      formdata.append("image",this.fileToUpload[0]);
      if (this.regForm.invalid) {
        return;
    }
      this.registreservice.addclient(formdata).subscribe((res:any)=>{
          console.log("response",res)
          Swal.fire("Client ajouté")
      })
      this.route.navigateByUrl('/')
      
  }

  onReset() {
      this.submitted = false;
      this.regForm.reset();
  }
}