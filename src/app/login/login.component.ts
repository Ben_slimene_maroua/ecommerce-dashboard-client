import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MustMatch } from 'helper/mustmatch';
import Swal from 'sweetalert2';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submitted=false;
logForm: FormGroup;
client:any;
  constructor(private loginservice:LoginService, private formbuilder: FormBuilder, private route:Router) { }

  ngOnInit(): void {
    this.logForm= this.formbuilder.group({
      email:['',[Validators.required, Validators.email]],
      password:['',[Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
    },
    {
      validator: MustMatch('password', 'confirmPassword')
    } );
  }
  get f() { return this.logForm.controls; }
 
Login(){
  this.submitted = true;
  if (this.logForm.invalid) {
    return;
  }
  return this.loginservice.login(this.logForm.value).subscribe((res:any)=>{
this.client=res['result']

  if (res.message==='login validé') {
    Swal.fire({
      icon:'success',
      title:'user found',
      text:'email valid',
      footer:'password valid'
    })
    this.route.navigateByUrl('/')
    localStorage.setItem('userconnect',JSON.stringify(res.user))
    localStorage.setItem('token',res.token)
    localStorage.setItem("state","0")
  }
 } ,err=>{
    Swal.fire({
      icon:'error',
      title:'user not found',
      text:'email invalid',
      footer:'password invalid'
    })
  }
)
}

}