import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-detailcart',
  templateUrl: './detailcart.component.html',
  styleUrls: ['./detailcart.component.css']
})
export class DetailcartComponent implements OnInit {
items=[] as any;
totitem:any;
i=1;
totalPrice:any;
item:any;
  constructor(private cartservice:CartService) { }

  ngOnInit(): void {
    this.getitems()
  }
getitems(){
  return this.items= this.cartservice.getItems()
}
total(item:any){

  let total=0;
  
    total = Number(item.prix)* Number(item.quantite);
  
  this.totitem=total;
  return this.totitem;

}
plus(){
  if(this.i < this.items.quantite){
    this.i++;
    this.items.quantite=this.i ;
  }
}
moins(){
  if(this.i !=1){
    this.i--;
    this.items.quantite=this.i ;
  }
}

gettotal(){
  let total=0;
  this.items.forEach((element:any) => {
    total += Number(element.prix)* Number(element.quantite);
  });
  this.totalPrice=total;
  return total;
    
  }
  removeitem(item:any){
    return this.cartservice.removeItem(item)
  }
}