import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { ListproductService } from '../listproduct.service';
import { Options } from 'ng5-slider';


@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.css']
})
export class ListproductComponent implements OnInit {
listprod:any;
search_nom:any;
p:number=1;
listcat:any;


//ng5 slider 
priceSelection=""
minvalue = 0;
maxvalue=2000;
options: Options = {
  floor: 0,
  ceil: 2000
};
  constructor(private listprodservice:ListproductService,private catservice: CategoryService ) { }

  ngOnInit(): void {
    this.getallproduct();
    this.getallCategories();
  }
getallproduct(){
  return this.listprodservice.getallproducts().subscribe((res:any)=>{
    
this.listprod=res['data'];
console.log(this.listprod);
  })
}


getallCategories(){
  return this.catservice.getcategory().subscribe((res:any)=>{
    this.listcat=res['data']
    console.log(this.listcat)
  })
}
// filtre par couleur
filtre(event:any){
  console.log("detected value color", event.target.value)
  this.listprodservice.getallproducts().subscribe((res:any)=>{
    this.listprod=res['data'].filter((element:any)=> element.color== event.target.value)
    console.log("listproduct color", this.listprod)
  })
}

//filtre par categorie
filtreCat(event:any){
  console.log("detected value categorie", event.target.value)
  this.listprodservice.getallproducts().subscribe((res:any)=>{
    this.listprod=res['data'].filter((element:any)=> element.subcat.category.nom== event.target.value)
    console.log("listproduct category", this.listprod)
  })
}

changePrice(){
  console.log("Price change", this.priceSelection)
  let event=this.priceSelection
  this.listprodservice.getallproducts().subscribe((res:any)=>{
    this.listprod=res['data']
    if(event !== undefined){
      const listprodByPrice=this.listprod  .filter((element:any)=> element.prix >= event[0] && element.prix <= event[1]);
      this.listprod= listprodByPrice
      console.log("listproduct price", this.listprod)
   }
   })
    }
}

