import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { CartService } from '../cart.service';
import { ListproductService } from '../listproduct.service';

@Component({
  selector: 'app-detailproduct',
  templateUrl: './detailproduct.component.html',
  styleUrls: ['./detailproduct.component.css']
})
export class DetailproductComponent implements OnInit {
  id=this.activeroute.snapshot.params['id'];
  product:any;
  qte_client:number=1;
  i=1;
  items=[] as any;
  constructor(private listprodservice:ListproductService, private activeroute: ActivatedRoute, private cartservice: CartService) { }

  ngOnInit(): void {
    this.getproducts();
  }
getproducts(){
  return this.listprodservice.getproduct(this.id).subscribe((res:any)=>{
    this.product= res['data'];
    console.log(this.product);
  })
}
plus(){
  
    this.i++;
    this.qte_client=this.i ;
  
}
moins(){
  if(this.i !=1){
    this.i--;
    this.qte_client=this.i ;
  }
}

addToCart(item:any){
  if(!this.cartservice.itemInCart(item)){
    item.quantite= this.qte_client
    this.cartservice.addTocart(item);
    this.items=this.cartservice.getItems()
      console.log(this.items)
    Swal.fire('product added')
  }
}
}
